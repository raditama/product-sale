const express = require('express');
const router = express.Router();
const cart = require('../models').cart;

module.exports = {
    getMethod: async (req, res) => {
        return cart
            .findAll()
            .then(cart => res.status(200).send(cart))
            .catch((error) => {
                console.log(error.toString());
                res.status(400).send(error);
            });
    },

    //===========================================================================================================
    // Multiply Post
    createMethod: async (req, res, next) => {
        let jumlahData = req.body.length;
        let arrResult = [];

        for (var i = 0; i < jumlahData; i++) {
            try {
                const body = new cart(req.body[i]);
                const result = await body.save();
                arrResult.push(result);
            } catch (error) {
                console.log(error.message);
                if (error.name === 'Validation Error') {
                    next(createError(422, error.message));
                    return;
                }
                next(error);
            }
        }
        res.send(arrResult);
    },

    updateMethod(req, res) {
        return cart
            .findByPk(req.params.id)
            .then((cart) => {
                if (!cart) {
                    return res.status(404).send({ message: "cart not found!" });
                }
                return cart
                    .update({
                        id_barang: req.body.id_barang || cart.id_barang,
                        nama_barang: req.body.nama_barang || cart.nama_barang,
                        kode: req.body.kode || cart.kode,
                        jumlah_stok: req.body.jumlah_stok || cart.jumlah_stok,
                        keterangan: req.body.keterangan || cart.keterangan,
                    })
                    .then(() => res.status(200).send({
                        message: "cart has been successfully updated!"
                    }))
                    .catch((error) => res.status(400).send({
                        message: "cart not found!"
                    }));
            })
            .catch((error) => res.status(400).send({
                message: "cart with id -> " + id + " not found!"
            }));
    },
    deleteMethod(req, res) {
        return cart
            .findByPk(req.params.id)
            .then((cart) => {
                if (!cart) {
                    return res.status(404).send({ message: "cart not found!" });
                }
                return cart
                    .destroy()
                    .then(() => res.status(200).send({
                        message: "cart has been successfully deleted!"
                    }))
                    .catch((error) => res.status(404).send({
                        message: "cart not found!"
                    }));
            })
            .catch((error) => res.status(400).send(error));
    }
}