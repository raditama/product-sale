const express = require('express');
const router = express.Router();
const products = require('../models').products;

module.exports = {
    getMethod: async (req, res) => {
        return products
            .findAll()
            .then(products => res.status(200).send(products))
            .catch((error) => {
                console.log(error.toString());
                res.status(400).send(error);
            });
    },
    createMethod: async (req, res, next) => {
        try {
            const body = new products(req.body);
            const result = await body.save();
            res.send(result);
        } catch (error) {
            console.log(error.message);
            if (error.name === 'Validation Error') {
                next(createError(422, error.message));
                return;
            }
            next(error);
        }
    },
    updateMethod(req, res) {
        return products
            .findByPk(req.params.id)
            .then((products) => {
                if (!products) {
                    return res.status(404).send({ message: "products not found!" });
                }
                return products
                    .update({
                        id_barang: req.body.id_barang || products.id_barang,
                        nama_barang: req.body.nama_barang || products.nama_barang,
                        kode: req.body.kode || products.kode,
                        jumlah_stok: req.body.jumlah_stok || products.jumlah_stok,
                        keterangan: req.body.keterangan || products.keterangan,
                    })
                    .then(() => res.status(200).send({
                        message: "products has been successfully updated!"
                    }))
                    .catch((error) => res.status(400).send({
                        message: "products not found!"
                    }));
            })
            .catch((error) => res.status(400).send({
                message: "products with id -> " + id + " not found!"
            }));
    },
    deleteMethod(req, res) {
        return products
        .findByPk(req.params.id)
        .then((products) => {
            if(!products){
                return res.status(404).send({message:"products not found!"});
            }
            return products
            .destroy()
            .then(() => res.status(200).send({
                message:"products has been successfully deleted!"
            }))
            .catch((error) => res.status(404).send({
                message:"products not found!"
            }));
        })
        .catch((error) => res.status(400).send(error));
    }
}