const express = require('express');
const router = express.Router();
const cartController = require('../controller/cart-controller');

router.get('/readAll', cartController.getMethod);

router.post('/create', cartController.createMethod);

router.put('/update/:id', cartController.updateMethod);

router.delete('/delete/:id', cartController.deleteMethod);

router.delete('/clear', cartController.clearMethod);

module.exports = router;