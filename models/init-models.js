var DataTypes = require("sequelize").DataTypes;
var _cart = require("./cart");
var _products = require("./products");

function initModels(sequelize) {
  var cart = _cart(sequelize, DataTypes);
  var products = _products(sequelize, DataTypes);


  return {
    cart,
    products,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
