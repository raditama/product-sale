const express = require('express');
const router = express.Router();
const productController = require('../controller/product-controller');

router.get('/readAll', productController.getMethod);

router.post('/create', productController.createMethod);

router.put('/update/:id', productController.updateMethod);

router.delete('/delete/:id', productController.deleteMethod);

module.exports = router;